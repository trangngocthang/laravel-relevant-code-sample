<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ProgramController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\PromoCodeController;
use Illuminate\Support\Facades\Route;
use Orion\Facades\Orion;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('user/login', [AuthController::class, 'login']);

Route::middleware('auth:sanctum')->group(function () {
    Route::post('user/register', [AuthController::class, 'register']);
    Route::get('user', [AuthController::class, 'info']);
    Route::post('user/logout', [AuthController::class, 'logout']);

    Route::patch('program/{program}/update-status', [ProgramController::class, 'updateStatus'])->name('program.update_status');
    Orion::resource('program', ProgramController::class);
    Route::get('service/default-area-items', [ServiceController::class, 'getDefaultAreaItems']);
    Route::patch('service/{service}/update-status', [ServiceController::class, 'updateStatus'])->name('service.update_status');
    Orion::resource('service', ServiceController::class);
    Orion::resource('client', ClientController::class);
    Route::get('client/potentials', [ClientController::class, 'index'])->name('client.potentials');
    Route::post('client/potentials/search', [ClientController::class, 'search'])->name('client.potentials.search');
    Route::get('client/potentials/{client}', [ClientController::class, 'show'])->name('client.potentials.show');
    Orion::resource('promoCode', PromoCodeController::class);
});
