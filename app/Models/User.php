<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class User - Client
 * @package App\Models
 * @property string name
 * @property string first_name
 * @property string last_name
 * @property string email
 * @property string phone
 * @property Charge[]|Collection charges
 * @property UserLawnDescription[]|Collection lawnDescriptions
 * @property UserLawnDescription[]|Collection potentialLawnDescriptions
 * @property UserLawnDescription[]|Collection activeLawnDescriptions
 */
class User extends Model
{
    protected $fillable = ['name', 'email'];

    public function charges() {
        return $this->hasMany(Charge ::class)->orderBy('created_at', 'desc');
    }

    public function lawnDescriptions() {
        return $this->hasMany(UserLawnDescription ::class)->orderBy('created_at', 'desc');
    }

    public function potentialLawnDescriptions() {
        return $this->hasMany(UserLawnDescription ::class)
            ->where('subscription_id', 0)
            ->orderBy('created_at', 'desc');
    }

    public function activeLawnDescriptions() {
        return $this->hasMany(UserLawnDescription ::class)
            ->where('subscription_id', '!=', 0)
            ->orderBy('created_at', 'desc');
    }
}
