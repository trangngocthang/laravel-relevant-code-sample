<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserLawnDescription
 * @package App\Models
 * @property string address
 * @property integer area
 * @property array issues
 * @property array goals
 * @property array aware_of
 * @property Charge charge
 * @property User $user
 * @property UserLawnDescriptionLineItem[]|Collection $lineItems
 */
class UserLawnDescription extends Model
{
    protected $table = 'user_lawn_description';

    protected $casts = [
        'issues' => 'array',
        'goals' => 'array',
        'aware_of' => 'array',
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function charge() {
        return $this->belongsTo(Charge::class,'subscription_id');
    }

    public function lineItems() {
        return $this->hasMany(UserLawnDescriptionLineItem::class,'user_lawn_description_id')->orderBy('created_at', 'desc');
    }

}
