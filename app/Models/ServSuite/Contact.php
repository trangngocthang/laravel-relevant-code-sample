<?php
namespace App\Models\ServSuite;

use Jenssegers\Mongodb\Eloquent\Model;

class Contact extends Model
{
    protected $connection = 'mongodb';

    protected $primaryKey = 'contactid';

    protected $guarded = [];
}
