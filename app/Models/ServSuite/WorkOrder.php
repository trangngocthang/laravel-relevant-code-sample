<?php
namespace App\Models\ServSuite;

use Jenssegers\Mongodb\Eloquent\Model;

class WorkOrder extends Model
{
    protected $connection = 'mongodb';

    protected $primaryKey = 'id';

    protected $guarded = [];
}
