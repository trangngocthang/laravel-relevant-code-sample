<?php
namespace App\Models\ServSuite;

use Jenssegers\Mongodb\Eloquent\Model;

class Estimate extends Model
{
    protected $connection = 'mongodb';

    protected $primaryKey = 'estimateid';

    protected $guarded = [];
}
