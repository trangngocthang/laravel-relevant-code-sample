<?php
namespace App\Models\ServSuite;

use Jenssegers\Mongodb\Eloquent\Model;

class Account extends Model
{
    protected $connection = 'mongodb';

    protected $primaryKey = 'accountid';

    protected $guarded = [];
}
