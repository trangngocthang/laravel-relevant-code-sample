<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Program
 * @package App\Models
 * @property string name
 * @property string description
 * @property integer type
 * @property integer department
 * @property string department_name
 * @property integer order
 * @property Service[]|Collection services
 */
class Program extends Model
{
    protected $table = 'product_tiers';

    protected $fillable = ['name', 'description', 'type', 'department', 'order'];

    public function services() {
        return $this->belongsToMany(Service ::class, 'product_tier_item', 'product_tier_id', 'product_item_id');
    }

    public static function getListTypes() {
        return [1 => 'Program', 2 => 'Addons'];
    }

    public static function getListDepartments() {
        return [1 => 'Law', 2 => 'Pest', 3 => 'Snow'];
    }

    public static function updateOrderOfAllPrograms() {
        $listDepartments = array_keys(static::getListDepartments());
        $listTypes = array_keys(static::getListTypes());
        foreach ($listDepartments as $department) {
            foreach ($listTypes as $type) {
                $updateOrderSql = <<<EOL
                    UPDATE product_tiers
                    SET `order` = (@rownum := 1 + @rownum)
                    WHERE 0 = (@rownum:=0) and department = {$department} and type = {$type}
                    ORDER BY `order` asc, updated_at desc;
EOL;

                DB::statement($updateOrderSql);
            }
        }
    }

    public function getDepartmentNameAttribute(): string
    {
        $departments = self::getListDepartments();
        return isset($departments[$this->department]) ? $departments[$this->department] : 'Unknown';
    }
}
