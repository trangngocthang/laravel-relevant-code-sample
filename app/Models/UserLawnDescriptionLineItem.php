<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserLawnDescriptionLineItem
 * @package App\Models
 * @property integer $group
 * @property string $product_tier_name
 * @property string $product_item_name
 * @property integer $type
 * @property integer $amount
 * @property Program $program
 * @property Service $service
 * @property UserLawnDescription $lawnDescription
 */
class UserLawnDescriptionLineItem extends Model
{

    public function program() {
        return $this->belongsTo(Program::class, 'product_tier_id');
    }

    public function service() {
        return $this->belongsTo(Service::class, 'product_item_id');
    }

    public function lawnDescription() {
        return $this->belongsTo(UserLawnDescription ::class,'user_lawn_description_id');
    }

}
