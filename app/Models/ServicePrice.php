<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductItemPrice
 * @package App\Models
 * @property string area_up_to
 * @property integer amount
 * @property string stripe_id
 * @property Service item
 */
class ServicePrice extends Model
{
    protected $table = 'product_item_prices';

    protected $fillable = ['amount', 'area_up_to'];

    public function service() {
        return $this->belongsTo(Service ::class,'product_item_id');
    }

    public static function getDefaultAreaItems() {
        return [3999, 7999, 12999, 15999, 19999, 23999, 27999, 32999, 39999];
    }

}
