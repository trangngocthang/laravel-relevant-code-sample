<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Charge
 * @package App\Models
 * @property integer subtotal_amount
 * @property integer tax_amount
 */
class Charge extends Model
{
    public function user() {
        return $this->belongsTo(User ::class);
    }
}
