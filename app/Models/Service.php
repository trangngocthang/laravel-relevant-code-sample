<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductItem
 * @package App\Models
 * @property string name
 * @property string description
 * @property string final_date
 * @property string stripe_id
 * @property Program[]|Collection programs
 * @property ServicePrice[]|Collection prices
 */
class Service extends Model
{
    protected $table = 'product_items';

    protected $fillable = ['name', 'description', 'final_date'];

    public function programs() {
        return $this->belongsToMany(Program ::class, 'product_tier_item', 'product_item_id', 'product_tier_id');
    }

    public function prices() {
        return $this->hasMany(ServicePrice ::class, 'product_item_id')->orderBy('area_up_to');
    }

    /**
     * @param Builder $query
     * @param $fromDate
     * @param $toDate
     */
    public function scopeWhereFinalDateBetween($query, $fromDate, $toDate) {
        if($fromDate && $toDate) {

            $query->whereNull('final_date')->orWhere(function($query) use ($fromDate, $toDate) {
                /** @var Builder $query */
                $query->where('final_date', '>=', $fromDate)
                    ->where('final_date', '<=', $toDate);
            });
        }
    }

    public static function boot() {
        parent::boot();

        static::deleting(function($model) {
            $model->prices()->delete();
        });
    }
}
