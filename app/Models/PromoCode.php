<?php
namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PromoCode
 * @package App\Models
 * @property string code
 * @property string description
 * @property int unit
 * @property int value
 * @property int expires_type
 * @property int limit_times
 * @property Carbon expires_date
 */
class PromoCode extends Model
{
    public const UNIT_CASH = 0;
    public const UNIT_PERCENT = 1;
    public const EXPIRE_TYPE_LIMIT_TIMES = 0;
    public const EXPIRE_TYPE_DATETIME = 1;

    protected $fillable = ['code', 'description', 'unit', 'value', 'expires_type', 'limit_times', 'expires_date'];

    protected $casts = [
        'expires_date' => 'datetime'
    ];

    public function services(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Service::class, 'promo_code_service_items', 'promo_code_id', 'product_item_id');
    }
}
