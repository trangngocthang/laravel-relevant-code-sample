<?php
namespace App\Policies;

use Illuminate\Database\Eloquent\Model;
use Orion\Concerns\HandlesAuthorization;

class ModelPolicy
{
    use HandlesAuthorization;

    public function viewAny(Model $model) {
        return true;
    }

    public function view(Model $model) {
        return true;
    }

    public function create(Model $model) {
        return true;
    }

    public function update(Model $model) {
        return true;
    }

    public function delete(Model $model) {
        return true;
    }

}
