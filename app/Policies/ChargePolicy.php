<?php
namespace App\Policies;

use Orion\Concerns\HandlesAuthorization;

class ChargePolicy extends ModelPolicy
{
    use HandlesAuthorization;

    public function create($model) {
        return false;
    }

    public function update($model) {
        return false;
    }

    public function delete($model) {
        return auth()->user()->isAdmin();
    }

}
