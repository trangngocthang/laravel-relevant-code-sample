<?php
namespace App\Policies;

use Orion\Concerns\HandlesAuthorization;

class ClientPolicy extends ModelPolicy
{
    use HandlesAuthorization;

    public function delete($model) {
        return false;
    }

}
