<?php
namespace App\Policies;

use Orion\Concerns\HandlesAuthorization;

class PromoCodePolicy extends ModelPolicy
{
    use HandlesAuthorization;
}
