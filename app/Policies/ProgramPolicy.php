<?php
namespace App\Policies;

use Orion\Concerns\HandlesAuthorization;

class ProgramPolicy extends ModelPolicy
{
    use HandlesAuthorization;

    public function delete($model) {
        return auth()->user()->isAdmin();
    }

}
