<?php

namespace App\Providers;

use App\Models\Charge;
use App\Models\Program;
use App\Models\PromoCode;
use App\Models\Service;
use App\Models\User;
use App\Policies\ChargePolicy;
use App\Policies\ClientPolicy;
use App\Policies\ProgramPolicy;
use App\Policies\PromoCodePolicy;
use App\Policies\ServicePolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Program::class => ProgramPolicy::class,
        Service::class => ServicePolicy::class,
        PromoCode::class => PromoCodePolicy::class,
        Charge::class => ChargePolicy::class,
        User::class => ClientPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
