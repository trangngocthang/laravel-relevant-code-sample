<?php
namespace App\Http\Services;

use Stripe\StripeClient;

class StripeService
{
    public $stripeClient;

    public function __construct()
    {
        $this->stripeClient = new StripeClient(config('services.stripe.secret'));
    }

}
