<?php

namespace App\Http\Resources\Client;

use App\Http\Resources\ChargeResource;
use App\Http\Resources\UserLawnDescriptionResource;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class ClientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        /** @var User|JsonResource $this */
        $data = [
            'id' => $this->id,
            'name' => $this->first_name ? "{$this->first_name} {$this->last_name}" : $this->name,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'phone' => $this->phone,
            'total_amount' => $this->total_amount,
            'total_tax' => $this->total_tax,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];

        if($request->route()->named('client.index', 'client.search')) {
            $query = <<<EOD
                select distinct(item_name) from charges
                left join charge_items on charge_items.charge_id = charges.id
                where product_item_id in (
                    select distinct(product_tier_item.product_item_id) from product_tier_item
                    left join product_tiers on product_tiers.id = product_tier_item.product_tier_id
                    where product_tiers.type = 2
                )
                and charges.user_id = :userId and item_name != ''
EOD;

            $data['purchased_addons'] = array_column(DB::select($query, ['userId' => $this->id]), 'item_name');
        }
        if($request->route()->named('client.show')) {
            $this->loadMissing(['charges', 'activeLawnDescriptions']);
            $data = array_merge($data, [
                'stripe_customer_id' => $this->charges()->value('customer'),
                'lawnDescriptions' => UserLawnDescriptionResource::collection($this->activeLawnDescriptions)    ,
                'charges' => ChargeResource::collection($this->whenLoaded('charges')),
            ]);
        }
        return $data;
    }
}
