<?php

namespace App\Http\Resources\Client;

use App\Http\Resources\UserLawnDescriptionResource;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class ClientPotentialResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        /** @var User $this */
        $this->loadMissing(['potentialLawnDescriptions', 'potentialLawnDescriptions.lineItems']);
        $data = [
            'id' => $this->id,
            'name' => $this->first_name ? "{$this->first_name} {$this->last_name}" : $this->name,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'phone' => $this->phone,
            'updated_at' => $this->updated_at,
        ];
        $data = array_merge($data, UserLawnDescriptionResource::getLatestLineItemsInfo($this->potentialLawnDescriptions[0]));

        if($request->route()->named('client.potentials.show')) {
            $data = array_merge($data, [
                'lawnDescriptions' => UserLawnDescriptionResource::collection($this->potentialLawnDescriptions)
            ]);
        }

        return $data;
    }
}
