<?php

namespace App\Http\Resources;

use App\Models\UserLawnDescription;
use Illuminate\Http\Resources\Json\JsonResource;

class UserLawnDescriptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        /** @var UserLawnDescription|JsonResource $this */
        $this->loadMissing('lineItems');
        $data = parent::toArray($request);
        list($latestEstimateAmount, $latestEstimateTax) = self::getLatestEstimateAmountAndTax($this->resource);
        $data['estimate_amount'] = $latestEstimateAmount;
        $data['estimate_tax'] = $latestEstimateTax;
        $data['historicalItems'] = $this->getHistorialItems();

        return $data;
    }

    public static function getLatestEstimateAmountAndTax($lawnDescription) {
        $latestEstimateAmount = null;
        $latestEstimateTax = null;
        if($lawnDescription && count($lawnDescription->lineItems) > 0) {
            $latestGroup = $lawnDescription->lineItems()->max('group');
            $latestLineItems = $lawnDescription->lineItems()->where('group', $latestGroup);
            $latestEstimateAmount = intval($latestLineItems->sum('amount'));
            $latestEstimateTax = round($latestEstimateAmount * 6 /100);
        }
        return [$latestEstimateAmount, $latestEstimateTax];
    }

    public static function getLatestLineItemsInfo($lawnDescription) {
        if($lawnDescription && count($lawnDescription->lineItems) > 0) {
            $latestGroup = $lawnDescription->lineItems()->max('group');
            list($latestEstimateAmount, $latestEstimateTax) = UserLawnDescriptionResource::getLatestEstimateAmountAndTax($lawnDescription);
            return [
                'program' => $lawnDescription->lineItems()->where('group', $latestGroup)->pluck('product_tier_name')->unique()->join(', '),
                'services' => $lawnDescription->lineItems()->where('group', $latestGroup)->get(['product_item_name', 'amount', 'type']),
                'address' => $lawnDescription->address,
                'area' => $lawnDescription->area,
                'issues' => $lawnDescription->issues ? $lawnDescription->issues : [],
                'goals' => $lawnDescription->goals ? $lawnDescription->goals : [],
                'aware_of' => $lawnDescription->aware_of ? $lawnDescription->aware_of : [],
                'estimate_amount' => $latestEstimateAmount,
                'estimate_tax' => $latestEstimateTax,
                'created_at' => $lawnDescription->lineItems()->where('group', $latestGroup)->first()->created_at
            ];
        }
        return [
            'address' => $lawnDescription->address,
            'area' => $lawnDescription->area,
            'issues' => [],
            'goals' => [],
            'aware_of' => [],
            'services' => [],
            'created_at' => $lawnDescription->created_at
        ];
    }

    /**
     * Group and format lineItems
     */
    protected function getHistorialItems() {
        /** @var UserLawnDescription|JsonResource $this */
        $historicalItems = [];
        $groups = $this->lineItems()->pluck('group')->unique();
        foreach ($groups as $group) {
            $estimateAmount = $this->lineItems()->where('group', $group)->sum('amount');
            $historicalItems[] = [
                'program' => $this->lineItems()->where('group', $group)->pluck('product_tier_name')->unique()->join(', '),
                'services' => $this->lineItems()->where('group', $group)->get(['product_item_name', 'amount', 'type']),
                'estimate_amount' => $estimateAmount,
                'estimate_tax' => round($estimateAmount * 6 /100),
                'created_at' => $this->lineItems()->where('group', $group)->first()->created_at,
            ];
        }
        return $historicalItems;
    }
}
