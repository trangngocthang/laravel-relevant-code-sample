<?php

namespace App\Http\Resources;

use App\Models\Service;
use Illuminate\Http\Resources\Json\JsonResource;

class ServiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $this->loadMissing('programs.services');
        /** @var Service|JsonResource $this */
        $data = parent::toArray($request);
        $lastItemInPrograms = [];
        foreach($this->programs as $program) {
            if($program->services()->where('status', 1)
                ->where('product_items.id', '!=', $this->id)->count() === 0) {
                $lastItemInPrograms[] = "{$program->department_name} - {$program->name}";
            }
        }
        $data['lastItemInPrograms'] = $lastItemInPrograms;
        return $data;
    }
}
