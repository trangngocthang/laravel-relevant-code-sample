<?php

namespace App\Http\Controllers;

use App\Http\Resources\Client\ClientPotentialResource;
use App\Http\Resources\Client\ClientResource;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Orion\Http\Requests\Request;

class ClientController extends OrionController
{
    protected $model = User::class;

    protected $resource = ClientResource::class;

    public function searchableBy() : array
    {
        $searchable = ['name', 'first_name', 'last_name', 'email'];
        if(request()->route()->named('client.potentials', 'client.potentials.search')) {
            $searchable[] = 'potentialLawnDescriptions.address';
        }
        return $searchable;
    }

    public function sortableBy(): array
    {
        return ['name', 'first_name', 'last_name', 'email', 'phone', 'created_at', 'updated_at'];
    }

    public function getResource(): string
    {
        if(request()->route()->named('client.potentials', 'client.potentials.search', 'client.potentials.show')) {
            return ClientPotentialResource::class;
        }
        return parent::getResource();
    }

    protected function buildFetchQuery(Request $request, array $requestedRelations): Builder
    {
        $sortingField = $request->post('sort', []) ? $request->post('sort', [])[0]['field'] : null;
        $sortingDirection = $request->post('sort', []) ? $request->post('sort', [])[0]['direction'] : null;
        $sortingCustomField = in_array($sortingField, ['total_amount', 'total_tax']);
        if($sortingCustomField) {
            $request->request->remove('sort');
        }
        $query = parent::buildFetchQuery($request, $requestedRelations);
        if($request->route()->named('client.potentials', 'client.potentials.search')) {
            $query->whereHas('lawnDescriptions', function($query) {
                /** @var Builder $query */
                $query->where('subscription_id', '=', 0);
            });
        } else if($request->route()->named('client', 'client.search')) {
            $query->whereHas('lawnDescriptions', function($query) {
                /** @var Builder $query */
                $query->where('subscription_id', '!=', 0);
            });
        }
        $query->withCount([
            'charges as total_amount' => function ($query) {
                /** @var Builder $query */
                $query->select(DB::raw('CAST(IFNULL(sum(subtotal_amount), 0) AS UNSIGNED) as total_amount'));
            },
            'charges as total_tax' => function ($query) {
                /** @var Builder $query */
                $query->select(DB::raw('CAST(IFNULL(sum(tax_amount), 0) AS UNSIGNED) as total_tax'));
            }
        ]);
        if($sortingCustomField) {
            $query->orderBy($sortingField, $sortingDirection);
        }
        return $query;
    }

}
