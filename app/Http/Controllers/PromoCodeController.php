<?php

namespace App\Http\Controllers;

use App\Http\Requests\PromoCodeRequest;
use App\Models\PromoCode;
use Orion\Http\Requests\Request;

class PromoCodeController extends OrionController
{
    protected $model = PromoCode::class;

    protected $request = PromoCodeRequest::class;

    public function searchableBy() : array
    {
        return ['code', 'description'];
    }

    public function sortableBy(): array
    {
        return ['code', 'value', 'expires_date', 'created_at', 'updated_at'];
    }

    public function alwaysIncludes(): array
    {
        return ['services'];
    }


    /**
     * @param Request $request
     * @param PromoCode $entity
     * @return mixed|void|null
     */
    protected function afterSave(Request $request, $entity)
    {
        $entity->services()->sync($request->post('services', []));
    }

    /**
     * @param Request $request
     * @param PromoCode $entity
     * @return mixed
     */
    protected function afterDestroy(Request $request, $entity)
    {
        $entity->services()->detach();
    }

}
