<?php

namespace App\Http\Controllers;

use Orion\Http\Controllers\Controller as BaseController;
use Orion\Http\Requests\Request;

class OrionController extends BaseController
{

    protected function shouldPaginate(Request $request, int $paginationLimit): bool
    {
        $page = $request->get('page');
        if ($page && (int)$page < 0) {
            return false;
        }
        return parent::shouldPaginate($request, $paginationLimit);
    }
}
