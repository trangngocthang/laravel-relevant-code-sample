<?php

namespace App\Http\Controllers;

use App\Http\Requests\ServiceRequest;
use App\Http\Services\StripeService;
use App\Models\Service;
use App\Models\ServicePrice;
use Illuminate\Support\Facades\DB;
use Orion\Http\Requests\Request;

class ServiceController extends OrionController
{
    protected $stripeService;

    public function __construct(StripeService $stripeService)
    {
        $this->stripeService = $stripeService;
        parent::__construct();
    }

    protected $model = Service::class;

    protected $request = ServiceRequest::class;

    public function searchableBy() : array
    {
        return ['name', 'description', 'programs.name'];
    }

    public function sortableBy(): array
    {
        return ['name', 'description', 'final_date', 'status', 'created_at', 'updated_at'];
    }

    public function filterableBy(): array
    {
        return ['status'];
    }

    public function includes(): array
    {
        return ['programs', 'prices'];
    }

    public function alwaysIncludes(): array
    {
        return ['programs'];
    }

    public function exposedScopes() : array
    {
        return ['whereFinalDateBetween'];
    }

    /**
     * @param Service $entity
     * @throws \Stripe\Exception\ApiErrorException
     */
    protected function beforeStore(Request $request, $entity)
    {
        $productData = [
            'name' => $request->get('name')
        ];
        if($request->get('description')) {
            $productData['description'] = $request->get('description');
        }
        $stripeProduct = $this->stripeService->stripeClient->products->create($productData);
        $entity->stripe_id = $stripeProduct->id;
    }

    /**
     * @param Request $request
     * @param Service $entity
     * @return mixed|void|null
     */
    protected function afterSave(Request $request, $entity)
    {
        $entity->programs()->sync($request->get('programs'));
        $postPrices = $request->get('prices');
        $prices = [];
        forEach($postPrices as $price) {
            /** @var ServicePrice $areaPrice */
            $areaPrice = $entity->prices()->where('area_up_to', $price['area_up_to'])->first();
            if(!$areaPrice) {
                $areaPrice = new ServicePrice($price);
                $priceData = [
                    'unit_amount' => $price['amount'],
                    'currency' => 'usd',
                    'product' => $entity->stripe_id,
                    'nickname' => "Area {$price['area_up_to']}",
                    'metadata' => ['area_up_to' => $price['area_up_to']]
                ];
                $stripePrice = $this->stripeService->stripeClient->prices->create($priceData);
                $areaPrice->stripe_id = $stripePrice->id;
            } else if($areaPrice->amount !== $price['amount']) {
                $this->stripeService->stripeClient->prices->update($areaPrice->stripe_id, ['active' => false]); // de-active old stripe price
                $priceData = [
                    'unit_amount' => $price['amount'],
                    'currency' => 'usd',
                    'product' => $entity->stripe_id,
                    'nickname' => "Area {$price['area_up_to']}",
                    'metadata' => ['area_up_to' => $price['area_up_to']]
                ];
                $stripePrice = $this->stripeService->stripeClient->prices->create($priceData);
                $areaPrice->stripe_id = $stripePrice->id;
                $areaPrice->amount = $price['amount'];
            }
            $prices[] = $areaPrice;
        }
        $deletePrices = $entity->prices()->whereNotIn('area_up_to', array_column($postPrices, 'area_up_to'))->get();
        foreach($deletePrices as $price) {
            $this->stripeService->stripeClient->prices->update($price->stripe_id, ['active' => false]); // de-active old stripe price
            $price->delete();
        }
        $entity->prices()->saveMany($prices);
    }

    /**
     * @param Request $request
     * @param Service $entity
     * @return mixed|void|null
     */
    protected function afterDestroy(Request $request, $entity)
    {
        $entity->programs()->detach();
        $this->stripeService->stripeClient->products->update($entity->stripe_id, ['active' => false]); // de-active stripe product
    }

    public function getDefaultAreaItems() {
        return response()->json(['data' => ServicePrice::getDefaultAreaItems()]);
    }

    public function updateStatus(Request $request, $key) {
        $request->validate([
            'status' => ['required', 'integer', 'in:0,1'
            ]]);
        /** @var Service $entity */
        $entity = Service::with(['programs', 'programs.services'])->findOrFail($key);
        DB::beginTransaction();
        try {
            $entity->status = $request->post('status');
            $entity->save();
            if ($entity->status === 0) {
                // Automatic inactive program belongs to
                foreach ($entity->programs as $program) {
                    if ($program->services()->where('status', 1)
                            ->where('product_items.id', '!=', $entity->id)->count() === 0) {
                        $program->status = 0; // set program status to inactive
                        $program->save();
                    }
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
        }
        return $this->entityResponse($entity);
    }

}
