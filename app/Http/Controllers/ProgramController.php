<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProgramRequest;
use App\Models\Program;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Orion\Http\Requests\Request;

class ProgramController extends OrionController
{
    protected $model = Program::class;

    protected $request = ProgramRequest::class;

    public function searchableBy() : array
    {
        return ['name', 'description'];
    }

    public function sortableBy(): array
    {
        return ['name', 'type', 'department', 'order', 'status', 'created_at', 'updated_at'];
    }

    public function filterableBy(): array
    {
        return ['status', 'department'];
    }

    public function alwaysIncludes(): array
    {
        return ['services'];
    }

    protected function beforeSave(Request $request, Model $entity)
    {
        $orderValue = $request->get('order');
        if(!$orderValue || $orderValue <= 0) {
            $entity->order = Program::query()->max('order') + 1;
        }
    }

    /**
     * @param Request $request
     * @param Program $entity
     * @return mixed|void|null
     */
    protected function afterSave(Request $request, $entity)
    {
        $entity->services()->sync($request->post('services', []));
        Program::updateOrderOfAllPrograms();
    }

    /**
     * @param Request $request
     * @param Program $entity
     * @return mixed
     */
    protected function afterDestroy(Request $request, $entity)
    {
        $entity->services()->detach();
    }

    protected function buildIndexFetchQuery(Request $request, array $requestedRelations): Builder
    {
        $query = $this->buildFetchQuery($request, $requestedRelations);
        if(!$request->post('sort')) {
            // default sort
            $query->orderBy('department')->orderBy('type')->orderBy('order');
        }
        return $query;
    }

    public function updateStatus(Request $request, $key) {
        $request->validate([
            'status' => ['required', 'integer', 'in:0,1'
        ]]);
        $entity = Program::query()->findOrFail($key);
        $entity->status = $request->post('status');
        $entity->save();
        return $this->entityResponse($entity);
    }

}
