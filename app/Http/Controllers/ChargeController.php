<?php

namespace App\Http\Controllers;

use App\Models\Charge;

class ChargeController extends OrionController
{

    protected $model = Charge::class;

    public function sortableBy(): array
    {
        return ['created_at'];
    }

}
