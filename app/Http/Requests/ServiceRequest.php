<?php
namespace App\Http\Requests;

use App\Models\ServicePrice;
use Illuminate\Validation\Rule;
use Orion\Http\Requests\Request;

class ServiceRequest extends Request
{

    public function commonRules(): array
    {
        return [
            'name' => ['required'],
            'final_date' => ['nullable', 'date'],
            'programs' => ['array'],
            'programs.*' => ['integer', 'exists:product_tiers,id'],
            'prices' => ['array', 'min:1'],
            'prices.*.amount' => ['integer', 'required'],
            'prices.*.area_up_to' => ['integer', 'required', Rule::in(ServicePrice::getDefaultAreaItems()) , 'distinct'],
        ];
    }
}
