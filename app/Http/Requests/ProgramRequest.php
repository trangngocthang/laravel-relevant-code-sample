<?php
namespace App\Http\Requests;

use App\Models\Program;
use Illuminate\Validation\Rule;
use Orion\Http\Requests\Request;

class ProgramRequest extends Request
{

    public function commonRules(): array
    {
        return [
            'name' => ['required'],
            'type' => ['required', Rule::in(array_keys(Program::getListTypes()))],
            'department' => ['required', Rule::in(array_keys(Program::getListDepartments()))],
            'order' => ['integer']
        ];
    }
}
