<?php
namespace App\Http\Requests;

use App\Models\Program;
use App\Models\PromoCode;
use Illuminate\Validation\Rule;
use Orion\Http\Requests\Request;

class PromoCodeRequest extends Request
{

    public function commonRules(): array
    {
        return [
            'code' => ['required'],
            'unit' => ['required', 'integer', Rule::in([PromoCode::UNIT_CASH, PromoCode::UNIT_PERCENT])],
            'value' => ['required', 'integer', 'min:1'],
            'expires_type' => ['required', 'integer', Rule::in([PromoCode::EXPIRE_TYPE_LIMIT_TIMES, PromoCode::EXPIRE_TYPE_DATETIME])],
        ];
    }

    /**
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if(array_search($this->route()->getActionMethod(), ['store', 'batchStore', 'update', 'batchUpdate'])) {
            $validator->sometimes('value', ['max:100'], function ($input) {
                return $input->unit == PromoCode::UNIT_PERCENT;
            });
            $validator->sometimes('limit_times', ['integer', 'required'], function ($input) {
                return $input->expires_type == PromoCode::EXPIRE_TYPE_LIMIT_TIMES;
            });
            $validator->sometimes('expires_date', ['date', 'required'], function ($input) {
                return $input->expires_type == PromoCode::EXPIRE_TYPE_DATETIME;
            });
        }
    }
}
